import serial
import os, time

port = serial.Serial("/dev/ttyUSB5", baudrate=115200, timeout=1)

port.write(b"AT\r")
rcv = port.read(10)
print(rcv)
time.sleep(1)

port.write(b"ATD6281945599444;\r")
print("Calling…")
time.sleep(30)
port.write(b"ATH\r")
print("Hang Call…")