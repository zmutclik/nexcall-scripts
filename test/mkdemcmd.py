from modemcmd import modemcmd
from modemcmd import ModemcmdException
from modemcmd import ModemcmdTimeoutException

try:
    result = modemcmd('/dev/ttyUSB0', 'AT+CGSN', 10)
    print(result)
except ModemcmdTimeoutException as e:
    print(e)
except ModemcmdException as e:
    print(e)
