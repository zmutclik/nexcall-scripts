#!/usr/bin/env python3

import urllib.request
import json
from pprint import pprint
import mysql.connector
import uuid

mydb = mysql.connector.connect(  host="172.17.0.5",  user="semut",  password="blackant")


url             = "https://api.livepreview.xyz/api/c1/get-transactions"
path_outgoing   = "/var/spool/sms/outgoing/"

values = {"jumlah_data" : "1","id_region" : "1"}

headers = {"Api-Token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyX2lkIjoxNCwidG9rZW5fbmFtZSI6IkpFTEFKQUggS09UQSAxIn0.Tln4IHwwIJkum_KQ_9PhSVBTUEfUybE3STNWJwJi_PAad-WgMBSpzNlXdjZczCkPBmxDch1u82PmpgOvqWeUDw",
        "content-type": "application/json",
        "Accept": "application/json",}

data = json.dumps(values).encode("utf-8")
responds = ""

try:
    req = urllib.request.Request(url, data, headers)
    with urllib.request.urlopen(req) as f:
        res = f.read()
    responds = json.loads(res.decode())
    pprint(responds)
except Exception as e:
    pprint(e)

def call_exec(myuuid, msisdn):
    f = open(path_outgoing + myuuid, "a")
    f.write("To: " + msisdn + "\n")
    f.write("Voicecall: yes\n")
    f.write("\n")
    f.write("TIME: 17\n")
    f.write("FILENAME: " + myuuid + "\n")
    f.close()

def call_save(data):
    try:
        mycursor = mydb.cursor()
        myuuid = str(uuid.uuid4())
        sql = "INSERT INTO `db`.`call_logs` (`id`, `uuid`, `tanggal`, `msisdn`, `delivery_mode`, `region`) VALUES (%s, %s, %s, %s, %s, %s);"
        val = (data[0]["trxid"], myuuid, data[0]["tanggal"]+" "+data[0]["jam"], data[0]["msisdn"], data[0]["delivery_mode"], data[0]["id_region"])
        mycursor.execute(sql, val)

        mydb.commit()

        call_exec(myuuid, data[0]["msisdn"])
        return True
    except mysql.connector.Error as error:
        print("Failed to insert into MySQL table {}".format(error))
        return False
    


if responds["status"]==200:
    call_save(responds["data"])
    

pprint(responds["status"])
