#!/usr/bin/env python3
from __future__ import print_function

import sys, time, logging

from gsmmodem.modem import GsmModem
from gsmmodem.exceptions import InterruptedException, CommandError
import io

PORT = '/dev/ttyUSB3'
BAUDRATE = 115200
NUMBER = '+6281945599444'

def handleNotif(notif):
    print('MYNOTIF : ' + notif)

def main():
    if NUMBER == None or NUMBER == '00000':
        print('Error: Please change the NUMBER variable\'s value before running this example.')
        sys.exit(1)
    print('Initializing modem...')
    logger = logging.getLogger('basic_logger')
    logger.setLevel(logging.DEBUG)
    logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.DEBUG)
    log_capture_string = io.StringIO()
    ch = logging.StreamHandler(log_capture_string)
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(format)
    ### Add the console handler to the logger
    logger.addHandler(ch)

    modem = GsmModem(PORT, BAUDRATE)
    modem.connect()
    print('Waiting for network coverage...')
    modem.waitForNetworkCoverage(20)
    print('Dialing number: {0}'.format(NUMBER))
    call = modem.dial(NUMBER)
    print('Waiting for Connecting to call . . .')
    wasAnswered = False
    while call.active:
        if call.answered:
            wasAnswered = True
            print('Call has been Connected; waiting a ring...')
            # Wait for a bit - some older modems struggle to send DTMF tone immediately after answering a call
            time.sleep(3.0)
            # print('Playing DTMF tones...')
            try:
                if call.active: # Call could have been ended by remote party while we waited in the time.sleep() call
                    # call.sendDtmfTone('9515999955951')
                    print('Caller Diangkat. . .')
                    time.sleep(5)
            except InterruptedException as e:
                # Call was ended during playback
                print('DTMF playback interrupted: {0} ({1} Error {2})'.format(e, e.cause.type, e.cause.code))
            except CommandError as e:
                print('DTMF playback failed: {0}'.format(e))
            finally:
                if call.active: # Call is still active
                    print('Hanging up call...')
                    call.hangup()
                else: # Call is no longer active (remote party ended it)
                    print('Call has been ended by remote party')

        elif call.callringing :
            print("Call Ringing BOZ")
        else:
            # Wait a bit and check again
            time.sleep(1)
            print("waiting")

    if not wasAnswered:
        print('Call was not answered by remote party')
    print('Done.')
    modem.close()


if __name__ == '__main__':
    main()
