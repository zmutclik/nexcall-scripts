#!/usr/bin/env python3

from sys import argv
from pprint import pprint
import mysql.connector

mydb        = mysql.connector.connect(  host="172.17.0.3",  user="semut",  password="blackant")

url         = "https://api.livepreview.xyz/api/c1/set-respon-requests"
headers     = {"Api-Token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyX2lkIjoxNCwidG9rZW5fbmFtZSI6IkpFTEFKQUggS09UQSAxIn0.Tln4IHwwIJkum_KQ_9PhSVBTUEfUybE3STNWJwJi_PAad-WgMBSpzNlXdjZczCkPBmxDch1u82PmpgOvqWeUDw","content-type": "application/json","Accept": "application/json",}

statuscode = argv[1]
smsfilename = argv[2]
smsfile = open(smsfilename)
smsdata = smsfile.read()
smsdata = smsdata.splitlines( )

smsdict = {}
for x in smsdata:
  x = x.partition(": ")
  if x[0]!="":
    smsdict[x[0].upper()] = x[2]
  #print(x)


try:
    mycursor = mydb.cursor()
    sql = "UPDATE db.call_logs SET modem=%s, sent=%s, sending_time=%s, imsi=%s, imei=%s, result=%s WHERE  uuid=%s;"
    val = (smsdict["MODEM"], smsdict["SENT"], smsdict["SENDING_TIME"], smsdict["IMSI"], smsdict["IMEI"], smsdict["RESULT"], smsdict["FILENAME"])
    print(smsdict["FILENAME"])
    mycursor.execute(sql, val)
    mydb.commit()
    print(mycursor.rowcount, "record(s) affected")
    print(mycursor.statement)
except mysql.connector.Error as error:
    print("Failed to insert into MySQL table {}".format(error))



values = {
	"trxid" : "3",
	"imei_sender" : smsdict["IMEI"],
	"id_region" : "1",
	"msisdn_sender" : smsdict["IMSI"],
	"rc_sender" : "201",
	"rc_message" : smsdict["RESULT"],
	"date_respon" : smsdict["SENDING_TIME"]
    }


try:
    mycursor = mydb.cursor()
    mycursor.execute("SELECT * FROM db.call_logs WHERE uuid=%s;",(smsdict["FILENAME"]))
    myresult = mycursor.fetchone()

    print(myresult)

except mysql.connector.Error as error:
    print("Failed Send Respond {}".format(error))
#pprint(smsdict)
